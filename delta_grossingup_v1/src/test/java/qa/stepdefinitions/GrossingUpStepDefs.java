package qa.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import qa.conf.DriverConfig;
import qa.tasks.*;

@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class GrossingUpStepDefs {

    @Autowired
    private NavigateTo navigateTo;

    @Autowired
    private MenuGrossingUp menuGrossingUp;

    @Autowired
    private CrearGrossingUp crearGrossingUp;

    @Autowired
    private ValidarCrearGrossingUp validarCrearGrossingUp;

    @Autowired
    private EditarVersionado editarVersionado;

    @Autowired
    private ValidarEditarVersionado validarEditarVersionado;

    @Autowired
    private EliminarVersionado eliminarVersionado;

    @Autowired
    private ValidarEliminarVersionado validarEliminarVersionado;

    @Before
    public void goHomePage() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(2000);
    }

    @Given("^actuario quiere ir al menu Grossing Up$")
    public void actuario_quiere_ir_al_menu_grossing_up() throws Throwable {
        menuGrossingUp.irMenuGrossingUp();
        Thread.sleep(2000);
    }

    @When("^actuario quiere crear grossing up con datos requeridos$")
    public void actuario_quiere_crear_grossing_up_con_datos_requeridos() throws Throwable {
        crearGrossingUp.withInfoRequired();
        Thread.sleep(2000);
    }

    @Then("^actuario tiene grossing up creado con datos requeridos$")
    public void actuario_tiene_grossing_up_creado_con_datos_requeridos() throws Throwable {
        Assert.assertTrue(true);
    }


}
