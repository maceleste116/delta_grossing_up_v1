package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomeGrossingUpPage extends PageBase {

    @Autowired
    public HomeGrossingUpPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="nabvar-burguer-menu-boton-desplegar")
    private WebElement menuPrincipal;

    @FindBy(id="nabvar-burguer-menu-boton-grossing-up")
    private WebElement menuGrossinUp;

    @FindBy(id="grossing-up-abm-tabla-boton-mostrar-acciones-0")
    private WebElement tablaMostrarAccion;

    @FindBy(id="grossing-up-abm-boton-crear")
    private WebElement buttonCrear;

    @FindBy(id="grossing-up-abm-boton-volver")
    private WebElement buttonVolverMenuPrincipal;

    @FindBy(id="grossing-up-abm-tabla-boton-editar-0")
    private WebElement tablaButtonEditar;

    @FindBy(id="grossing-up-abm-tabla-boton-eliminar-0")
    private WebElement tablaButtonEliminar;



    //TABLA

    @FindBy(id="grossing-up-abm-tabla")
    private WebElement mostrarTableHome;

    @FindBy(id="grossing-up-abm-tabla-icono-orden-nombre")
    private WebElement iconSortAsc;

    @FindBy(id="grossing-up-abm-tabla-icono-orden-nombre")
    private WebElement iconSortDesc;







}
