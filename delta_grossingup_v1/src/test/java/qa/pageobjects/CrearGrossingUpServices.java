package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CrearGrossingUpServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private CrearGrossingUpPage crearGrossingUpPage;

    public void writeInputCrearNombre(String nombre){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getInputCrearNombre()));
        this.crearGrossingUpPage.getInputCrearNombre().sendKeys(nombre);
    }

    public void writeInputCrearVersion(String version){
        this.crearGrossingUpPage.getInputCrearVersion().sendKeys(version);
    }

    public void selectCrearEstado(String estado) throws InterruptedException {
        this.crearGrossingUpPage.getSelectCrearEstado().click();
        List<WebElement> options = this.crearGrossingUpPage.getSelectCrearEstado().findElements(By.tagName("options"));
        for (WebElement option : options) {
            if (estado.equals(option.getText()))
                option.isSelected();
                option.click();
        }

    }

    public void writeInputCrearGasto(String gasto){
        this.crearGrossingUpPage.getInputCrearGasto().sendKeys(gasto);
    }

    public void writeInputCrearBonificacion(String bonificacion){
        this.crearGrossingUpPage.getInputCrearBonificacion().sendKeys(bonificacion);
    }

    public void writeInputCrearGap(String gap){
        this.crearGrossingUpPage.getInputCrearGap().sendKeys(gap);
    }

    public void selectCrearRamo(String ramo){
        this.crearGrossingUpPage.getSelectCrearRamo().click();
        List<WebElement> options = this.crearGrossingUpPage.getSelectCrearRamo().findElements(By.tagName("options"));

        for (WebElement option : options) {
            if(ramo.equals(option.getText()))
                option.isSelected();
                option.click();
        }


    }


    public void writeInputCrearFechaHora(String fecha, String hora){
        this.crearGrossingUpPage.getInputCrearFecha().sendKeys(fecha);
        this.crearGrossingUpPage.getInputCrearFecha().sendKeys("Keys.TAB");
        this.crearGrossingUpPage.getInputCrearHora().sendKeys(hora);
    }

    public void writeInputCrearComision(String comision){
        this.crearGrossingUpPage.getInputCrearComision().sendKeys(comision);
    }

    public void writeInputCrearSiniestroObjetivo(String siniestroObjetivo){
        this.crearGrossingUpPage.getInputCrearSiniestroObjetivo().sendKeys(siniestroObjetivo);
    }

    public void writeInputCrearRecargo(String recargo){
        this.crearGrossingUpPage.getInputCrearRecargo().sendKeys(recargo);
    }

    public void writeInputCrearFactorGup(String factorGup){
        this.crearGrossingUpPage.getInputCrearFactorGupManual().sendKeys(factorGup);
    }

    public void clickOnButtonCalcular() {
        this.crearGrossingUpPage.getButtonCalcular().click();
    }

    public void clickOnButtonCancelar() {

        this.crearGrossingUpPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.crearGrossingUpPage.getButtonConfirmar().click();
    }

    public String getSiniestroNoObjetivo(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getInputCrearSiniestroNoObjetivo()));
        return this.crearGrossingUpPage.getInputCrearSiniestroNoObjetivo().getText();
    }

    public String getResultadoTecnicoNoObjetivo(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getInputCrearResultadoTecnicoNoObjetivo()));
        return this.crearGrossingUpPage.getInputCrearResultadoTecnicoNoObjetivo().getText();
    }

    public String getResultadoTecnicoObjetivo(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getInputCrearResultadoTecnicoObjetivo()));
        return this.crearGrossingUpPage.getInputCrearResultadoTecnicoObjetivo().getText();
    }

    public String getFactorGupCalculado(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getInputCrearFactorGupCalculado()));
        return this.crearGrossingUpPage.getInputCrearFactorGupCalculado().getText();
    }

    public String getMensajeCrearExito(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getMensajeCrearExitosa()));
        return this.crearGrossingUpPage.getMensajeCrearExitosa().getText();
    }

    public String getMensajeFaltanCampos(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getMensajeCrearFaltanCampos()));
        return this.crearGrossingUpPage.getMensajeCrearFaltanCampos().getText();
    }

    public String getMensajeNoGuardar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearGrossingUpPage.getMensajeCrearNoGuardar()));
        return this.crearGrossingUpPage.getMensajeCrearNoGuardar().getText();
    }

    // Falta otra opción de guardado rerlacionado con cálculos



    }



