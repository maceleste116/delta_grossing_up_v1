package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CrearGrossingUpPage extends PageBase {

    @Autowired
    public CrearGrossingUpPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="crear-grossing-up-input-nombreGrossingUp")
    private WebElement inputCrearNombre;

    @FindBy(id="crear-grossing-up-input-version")
    private WebElement inputCrearVersion;

    @FindBy(xpath="//*[@id='crear-grossing-up-select-estado']/p")
    private WebElement selectCrearEstado;

    @FindBy(id="crear-grossing-up-input-GASTOS")
    private WebElement inputCrearGasto;

    @FindBy(id="crear-grossing-up-input-BONIFICACION")
    private WebElement inputCrearBonificacion;

    @FindBy(id="crear-grossing-up-input-GAP")
    private WebElement inputCrearGap;

    @FindBy(css="#crear-grossing-up-select-codigoRamo>p")
    private WebElement selectCrearRamo;

    @FindBy(id="crear-grossing-up-date-picker-aplicaDesde-fecha")
    private WebElement inputCrearFecha;

    @FindBy(id="crear-grossing-up-date-picker-aplicaDesde-hora")
    private WebElement inputCrearHora;

    @FindBy(id="crear-grossing-up-input-COMISION")
    private WebElement inputCrearComision;

    @FindBy(id="crear-grossing-up-input-SINIESTROS_OBJETIVOS")
    private WebElement inputCrearSiniestroObjetivo;

    @FindBy(id="crear-grossing-up-input-RECARGOS")
    private WebElement inputCrearRecargo;

    @FindBy(id="crear-grossing-up-input-FACTOR_GUP_MANUAL")
    private WebElement inputCrearFactorGupManual;

    @FindBy(id="crear-grossing-up-input-SINIESTROS_NO_OBJETIVOS")
    private WebElement inputCrearSiniestroNoObjetivo;

    @FindBy(id="crear-grossing-up-input-RESULTADO_TECNICO_NO_OBJETIVO")
    private WebElement inputCrearResultadoTecnicoNoObjetivo;

    @FindBy(id="crear-grossing-up-input-RESULTADO_TECNICO_OBJETIVO")
    private WebElement inputCrearResultadoTecnicoObjetivo;

    @FindBy(id="crear-grossing-up-input-FACTOR_GUP")
    private WebElement inputCrearFactorGupCalculado;

    @FindBy(id="crear-grossing-up-boton-Calcular")
    private WebElement buttonCalcular;

    @FindBy(id="crear-grossing-up-boton-Cancelar")
    private WebElement buttonCancelar;

    @FindBy(id="crear-grossing-up-boton-Confirmar")
    private WebElement buttonConfirmar;

    @FindBy(id="crear-grossing-up-modal-mensaje")
    private WebElement mensajeCrearFaltanCampos; //Faltan campos

    @FindBy(id="crear-grossing-up-modal-mensaje")
    private WebElement mensajeCrearNoGuardar; //No se guardó el grossing up

    @FindBy(id="modal-mensaje")
    private WebElement mensajeCrearIngresarDatosCalculo; //Ingrese los datos necesarios para realizar el cálculo

    @FindBy(id="grossing-up-modal-mensaje")
    private WebElement mensajeCrearExitosa; // Se creó grossing up xxxguptest01xxx con éxito



}
