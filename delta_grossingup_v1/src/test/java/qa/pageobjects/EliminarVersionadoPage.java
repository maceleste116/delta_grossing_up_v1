package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.tasks.EliminarVersionado;

@Component
@Getter
public class EliminarVersionadoPage extends PageBase {

    @Autowired
    public EliminarVersionadoPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="modal-modal-button-confimar")
    private WebElement ButtonBorrar;

    @FindBy(id="modal-modal-button-cancelar")
    private WebElement ButtonCancelar;

    @FindBy(id="versionados-mensaje")
    private WebElement mensajeBorrar;


}
