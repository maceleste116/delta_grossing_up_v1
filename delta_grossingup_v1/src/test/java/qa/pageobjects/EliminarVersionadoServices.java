package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EliminarVersionadoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EliminarVersionadoPage eliminarVersionadoPage;

    @Autowired
    private HomeGrossingUpPage homeGrossingUpPage;

    public void clickOnTablaButtonBorrar() {
        this.wait.until(ExpectedConditions.visibilityOf(eliminarVersionadoPage.getButtonBorrar()));
        this.eliminarVersionadoPage.getButtonBorrar().click();
    }

    public void clickOnTablaButtonCancelar() {
        this.wait.until(ExpectedConditions.visibilityOf(eliminarVersionadoPage.getButtonBorrar()));
        this.eliminarVersionadoPage.getButtonBorrar().click();
    }

    public String getMensajeBorrar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.eliminarVersionadoPage.getMensajeBorrar()));
        return this.eliminarVersionadoPage.getMensajeBorrar().getText();
    }


}
