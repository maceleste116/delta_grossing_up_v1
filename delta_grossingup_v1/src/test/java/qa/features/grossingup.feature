Feature: Administrar Grossing Up

  Background: Ir al menu Grossing Up
    Given actuario quiere ir al menu Grossing Up

  Scenario: Crear grossing up con datos requeridos
    When actuario quiere crear grossing up con datos requeridos
    Then actuario tiene grossing up creado con datos requeridos

  Scenario: Crear grossing up sin datos requeridos
    When actuario quiere crear grossing up sin datos requeridos
    Then actuario no tiene grossing up creado sin datos requeridos

  Scenario: Crear grossing up con version existente--VER
    When actuario quiere crear grossing up con version existente
    Then actuario no tiene grossing up creado con version existente ---

  Scenario: Editar grossing up existente en estado edicion---
    When actuario quiere editar grossing up existente en estado edicion---
    Then actuario tiene grossing up editado en estado edicion---



  Scenario: Eliminar grossing up existente estado edición---
    When actuario quiere eliminar grossing up estado edicion---
    Then actuario tiene grossing up eliminado estado edición---



  Scenario: Listar ascendente grossing up existente
    When actuario quiere ordenar ascendente grossing up existente
    Then actuario tiene ordenado ascendente grossing up existente

  Scenario: Listar descendente grossing up existente
    When actuario quiere ordenar descendente grossing up existente
    Then actuario tiene ordenado descendente grossing up existente
