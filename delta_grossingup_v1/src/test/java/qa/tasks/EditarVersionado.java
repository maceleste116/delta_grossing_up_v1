package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarVersionadoServices;
import qa.pageobjects.HomeGrossingUpServices;

@Component
public class EditarVersionado {

    @Autowired
    private HomeGrossingUpServices homeGrossingUpServices;

    @Autowired
    private EditarVersionadoServices editarVersionadoServices;

    public void withEstadoEdición() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        editarVersionadoServices.writeEditarCrearFechaHora("07/08/2020","16:00");
        Thread.sleep(1000);
        editarVersionadoServices.selectEditarTarifaBase("90012");
        editarVersionadoServices.selectEditarTarifaFactor("90012");
        editarVersionadoServices.selectEditarMapeo("2");
        editarVersionadoServices.selectGrupoTarifario("GT7000");
        editarVersionadoServices.selectTarifaAdicional("90012");
        editarVersionadoServices.selectTarifaAccesoria("90012");
        editarVersionadoServices.clickOnButtonConfirmar();

    }
    public void withEstadoPublicada() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        //editarVersionadoServices.writeEditarCrearFechaHora("07/08/2020","16:00");
        //Thread.sleep(1000);
        //editarVersionadoServices.selectEditarTarifaBase("90012");
        //editarVersionadoServices.selectEditarTarifaFactor("90012");
        //editarVersionadoServices.selectEditarMapeo("2");
        //editarVersionadoServices.selectGrupoTarifario("GT7000");
        //editarVersionadoServices.selectTarifaAdicional("90012");
        editarVersionadoServices.selectTarifaAccesoria("90012");
        editarVersionadoServices.clickOnButtonConfirmar();

    }

    public void withEstadoInactiva() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        //editarVersionadoServices.writeEditarCrearFechaHora("07/08/2020","16:00");
        //Thread.sleep(1000);
        //editarVersionadoServices.selectEditarTarifaBase("90012");
        //editarVersionadoServices.selectEditarTarifaFactor("90012");
        //editarVersionadoServices.selectEditarMapeo("2");
        //editarVersionadoServices.selectGrupoTarifario("GT7000");
        //editarVersionadoServices.selectTarifaAdicional("90012");
        editarVersionadoServices.selectTarifaAccesoria("90012");
        editarVersionadoServices.clickOnButtonConfirmar();

    }

    public void publicarEstadoEdiciónPublicada() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        editarVersionadoServices.clickOnButtonPublicar();

    }

    public void publicarEstadoEdiciónFechaAnterior() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEditar();
        Thread.sleep(3000);
        editarVersionadoServices.writeEditarCrearFechaHora("01/08/2020","16:00");
        editarVersionadoServices.selectTarifaAccesoria("90011");
        editarVersionadoServices.clickOnButtonPublicar();

    }

}
