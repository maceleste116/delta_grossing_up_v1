package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarVersionadoServices;
import qa.pageobjects.EliminarVersionadoServices;

@Component
public class ValidarEliminarVersionado {

    @Autowired
    private EliminarVersionadoServices eliminarVersionadoServices;

    public boolean validarVersionadoEliminarWithEstadoEdicion(){
        String mensaje = eliminarVersionadoServices.getMensajeBorrar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se eliminó versionado");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarVersionadoEliminarWithEstadoPublicada(){
        String mensaje = eliminarVersionadoServices.getMensajeBorrar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("No es posible eliminar un versionado Publicado");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarVersionadoEliminarWithEstadoInactiva(){
        String mensaje = eliminarVersionadoServices.getMensajeBorrar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("No es posible eliminar un versionado Inactivo");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }



}
