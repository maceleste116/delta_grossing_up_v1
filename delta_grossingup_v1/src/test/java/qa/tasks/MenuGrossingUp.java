package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeGrossingUpServices;

@Component
public class MenuGrossingUp {
    @Autowired
    private HomeGrossingUpServices homeGrossingUpServices;

    public void irMenuGrossingUp(){
        homeGrossingUpServices.clickOnMenuPrincipal();
        homeGrossingUpServices.clickOnMenuGrossingUp();
    }
}
