package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EliminarVersionadoServices;
import qa.pageobjects.HomeGrossingUpServices;

@Component
public class EliminarVersionado {

    @Autowired
    private HomeGrossingUpServices homeGrossingUpServices;

    @Autowired
    private EliminarVersionadoServices eliminarVersionadoServices;

    public void withEstadoEdiciónPublicadaInactiva() throws InterruptedException {
        homeGrossingUpServices.clickOnTablaMostrarAccion();
        homeGrossingUpServices.clickOnTablaButtonEliminar();
        Thread.sleep(1000);
        eliminarVersionadoServices.clickOnTablaButtonBorrar();

    }


}
