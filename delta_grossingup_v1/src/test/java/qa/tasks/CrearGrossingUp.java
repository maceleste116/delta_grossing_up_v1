package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearGrossingUpServices;
import qa.pageobjects.HomeGrossingUpServices;

@Component
public class CrearGrossingUp {

    @Autowired
    private HomeGrossingUpServices homeGrossingUpServices;

    @Autowired
    private CrearGrossingUpServices crearGrossingUpServices;

    public void withInfoRequired() throws InterruptedException {
        homeGrossingUpServices.clickOnButtonCrear();
        crearGrossingUpServices.writeInputCrearNombre("GUP TEST ");
        crearGrossingUpServices.selectCrearRamo("Automotores");
        crearGrossingUpServices.writeInputCrearVersion("082020.01");
        crearGrossingUpServices.writeInputCrearFechaHora("20/08/2020","17:00");
        crearGrossingUpServices.selectCrearEstado("Publicada");
        crearGrossingUpServices.writeInputCrearComision("25");
        crearGrossingUpServices.writeInputCrearGasto("20");
        crearGrossingUpServices.writeInputCrearSiniestroObjetivo("60");
        crearGrossingUpServices.writeInputCrearBonificacion("20");
        crearGrossingUpServices.writeInputCrearRecargo("15");
        crearGrossingUpServices.writeInputCrearGap("0");
        crearGrossingUpServices.clickOnButtonCalcular();
        crearGrossingUpServices.clickOnButtonConfirmar();
    }

    /*public void withOutInfoRequired(){
        homeGrossingUpServices.clickOnButtonCrear();
        crearVersionadoServices.writeInputCrearVersion("");
        crearVersionadoServices.writeInputCrearFechaHora("dd/mm/aaaa","--:--");
        crearVersionadoServices.selectTarifaBase("Seleccione una opción");
        crearVersionadoServices.selectTarifaFactor("Seleccione una opción");
        crearVersionadoServices.selectMapeo("Seleccione una opción");
        crearVersionadoServices.selectGrupoTarifario("Seleccione una opción");
        //crearVersionadoServices.selectEstado("Editando");
        crearVersionadoServices.selectTarifaAdicional("Seleccione una opción");
        crearVersionadoServices.selectTarifaAccesoria("Seleccione una opción");
        crearVersionadoServices.clickOnButtonConfirmar();
    }

    public void withVersionExistente(){
        homeGrossingUpServices.clickOnButtonCrear();
        crearVersionadoServices.writeInputCrearVersion("122047.01");
        crearVersionadoServices.writeInputCrearFechaHora("05/08/2020","12:00");
        crearVersionadoServices.selectTarifaBase("90012");
        crearVersionadoServices.selectTarifaFactor("90012");
        crearVersionadoServices.selectMapeo("1");
        crearVersionadoServices.selectGrupoTarifario("GTEST");
        //crearVersionadoServices.selectEstado("Editando");
        crearVersionadoServices.selectTarifaAdicional("90012");
        crearVersionadoServices.selectTarifaAccesoria("90012");
        crearVersionadoServices.clickOnButtonConfirmar();
    }
*/
}
